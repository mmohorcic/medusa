/**
 * @file
 * Instantiations of commonly used WLS approximations.
 */

#include <medusa/bits/approximations/WLS.hpp>

#include <medusa/bits/approximations/Gaussian.hpp>
#include <medusa/bits/approximations/Multiquadric.hpp>
#include <medusa/bits/approximations/RBFBasis.hpp>
#include <medusa/bits/approximations/Monomials.hpp>
#include <medusa/bits/approximations/WeightFunction.hpp>
#include <medusa/bits/approximations/ScaleFunction.hpp>
#include <medusa/bits/approximations/JacobiSVDWrapper.hpp>
#include <medusa/bits/types/Vec.hpp>

#include <Eigen/Cholesky>
#include <Eigen/LU>
#include <Eigen/SVD>

namespace mm {

template class WLS<Monomials<Vec2d>>;
template class WLS<Gaussians<Vec2d>>;

template class WLS<Monomials<Vec1d>, NoWeight<Vec1d>, ScaleToFarthest>;
template class WLS<Gaussians<Vec1d>, NoWeight<Vec1d>, ScaleToFarthest>;
template class WLS<Monomials<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest>;
template class WLS<Monomials<Vec3d>, NoWeight<Vec3d>, ScaleToFarthest>;

template class WLS<Monomials<Vec1d>, GaussianWeight<Vec1d>, ScaleToFarthest>;
template class WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, ScaleToFarthest>;
template class WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest>;
template class WLS<Monomials<Vec3d>, GaussianWeight<Vec3d>, ScaleToFarthest>;

template class WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>>;
template class WLS<Gaussians<Vec2d>, GaussianWeight<Vec2d>>;
template class WLS<Gaussians<Vec2d>, GaussianWeight<Vec2d>, ScaleToFarthest>;
template class WLS<Gaussians<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest>;
template class WLS<Gaussians<Vec3d>, GaussianWeight<Vec3d>, ScaleToFarthest>;
template class WLS<Gaussians<Vec3d>, NoWeight<Vec3d>, ScaleToFarthest>;
template class WLS<Gaussians<Vec3d>, GaussianWeight<Vec3d>, ScaleToClosest>;


template class WLS<Gaussians<Vec2d>, NoWeight<Vec2d>,
        NoScale, Eigen::PartialPivLU<Eigen::MatrixXd>>;
template class WLS<Gaussians<Vec2d>, NoWeight<Vec2d>,
        ScaleToFarthest, Eigen::PartialPivLU<Eigen::MatrixXd>>;
template class WLS<Monomials<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest,
        Eigen::PartialPivLU<Eigen::MatrixXd>>;

template class WLS<MQs<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest>;
template class WLS<MQs<Vec3d>, GaussianWeight<Vec3d>, ScaleToClosest>;

template class WLS<Gaussians<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest,
        Eigen::LLT<Eigen::MatrixXd>>;
template class WLS<Gaussians<Vec3d>, NoWeight<Vec3d>, ScaleToFarthest,
        Eigen::LLT<Eigen::MatrixXd>>;
template class WLS<Gaussians<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest>;

}  // namespace mm
