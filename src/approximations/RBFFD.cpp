/**
 * @file
 * Instantiations of common RBFFD approximations.
 */

#include <medusa/bits/approximations/RBFFD.hpp>

#include <medusa/bits/approximations/Gaussian.hpp>
#include <medusa/bits/approximations/ScaleFunction.hpp>
#include <medusa/bits/approximations/Multiquadric.hpp>
#include <medusa/bits/approximations/InverseMultiquadric.hpp>
#include <medusa/bits/approximations/Polyharmonic.hpp>
#include <medusa/bits/approximations/RBFBasis.hpp>

#include <medusa/bits/types/Vec.hpp>
#include <Eigen/LU>

namespace mm {

template class RBFFD<Gaussian<double>, Vec1d, ScaleToClosest>;
template class RBFFD<Polyharmonic<double, 3>, Vec1d, ScaleToClosest>;

template class RBFFD<Gaussian<double>, Vec2d, ScaleToClosest>;
template class RBFFD<Gaussian<double>, Vec3d, ScaleToClosest>;

template class RBFFD<Polyharmonic<double, 3>, Vec3d, ScaleToClosest>;
template class RBFFD<Polyharmonic<double, 5>, Vec3d, ScaleToClosest>;
template class RBFFD<Polyharmonic<double, 7>, Vec3d, ScaleToClosest>;

template class RBFFD<Polyharmonic<double, 1>, Vec2d, ScaleToClosest>;
template class RBFFD<Polyharmonic<double, 3>, Vec2d, ScaleToClosest>;
template class RBFFD<Polyharmonic<double, 5>, Vec2d, ScaleToClosest>;
template class RBFFD<Polyharmonic<double, 7>, Vec2d, ScaleToClosest>;

template class RBFFD<Multiquadric<double>, Vec2d, ScaleToClosest>;

}  // namespace mm
