#include "medusa/bits/approximations/Multiquadric.hpp"

/// @cond
template class mm::Multiquadric<double>;
template std::ostream& mm::operator<<(std::ostream& os, const mm::Multiquadric<double>&);
/// @endcond
