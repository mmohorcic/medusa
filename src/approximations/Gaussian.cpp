#include <medusa/bits/approximations/Gaussian.hpp>

/**
 * @file
 * Instantiation of most commonly used Gaussian RBF.
 */

/// @cond
template class mm::Gaussian<double>;
template std::ostream& mm::operator<<(std::ostream& os, const mm::Gaussian<double>&);
/// @endcond
