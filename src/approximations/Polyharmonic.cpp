#include "medusa/bits/approximations/Polyharmonic.hpp"

/// @cond
template class mm::Polyharmonic<double>;
template std::ostream& mm::operator<<(std::ostream& os, const mm::Polyharmonic<double>&);

template class mm::Polyharmonic<double, 1>;
template std::ostream& mm::operator<<(std::ostream& os, const mm::Polyharmonic<double, 1>&);

template class mm::Polyharmonic<double, 3>;
template std::ostream& mm::operator<<(std::ostream& os, const mm::Polyharmonic<double, 3>&);

template class mm::Polyharmonic<double, 5>;
template std::ostream& mm::operator<<(std::ostream& os, const mm::Polyharmonic<double, 5>&);

template class mm::Polyharmonic<double, 7>;
template std::ostream& mm::operator<<(std::ostream& os, const mm::Polyharmonic<double, 7>&);
/// @endcond
