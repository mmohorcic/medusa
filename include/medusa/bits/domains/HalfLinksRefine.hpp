#ifndef MEDUSA_BITS_DOMAINS_HALFLINKSREFINE_HPP_
#define MEDUSA_BITS_DOMAINS_HALFLINKSREFINE_HPP_

/**
 * @file
 * Implementation of the half-link refinement algorithm.
 */


#include "HalfLinksRefine_fwd.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <medusa/bits/spatial_search/KDTree.hpp>
#include <medusa/bits/types/Range.hpp>
#include <numeric>

namespace mm {

template <typename vec_t>
Range<int> HalfLinksRefine::operator()(DomainDiscretization<vec_t>& domain) const  {
    Range<int> region = region_;
    if (region.empty()) { region = domain.all(); }

    int N = domain.size();
    typedef typename vec_t::scalar_t scalar_t;

    KDTree<vec_t> domain_tree(domain.positions());
    // sort: boundary nodes first
    std::sort(region.begin(), region.end(),
              [&](int i, int j) { return domain.type(i) < domain.type(j); });

    KDTree<vec_t> region_tree(domain.positions()[region]);

    // Remember which nodes added which points.
    Range<Range<int>> children(region.size());

    // Iterate through points in region and generate new points
    scalar_t max_support_radius = 0;
    for (int i = 0; i < N; ++i) {
        if (!domain.support(i).empty()) {
            int last = domain.support(i).back();
            scalar_t support_radius = (domain.pos(i) - domain.pos(last)).norm();
            if (support_radius > max_support_radius) {
                max_support_radius = support_radius;
            }
        }
    }
    assert_msg(max_support_radius > 0, "Max support radius should be positive, got 0. Did you"
                                       " forget to find support before refining?");
    max_support_radius = std::sqrt(max_support_radius);
    return refine_impl(domain, region, 0, fraction_, domain_tree, region_tree,
                       children, max_support_radius);
}

template <class vec_t>
Range<int> HalfLinksRefine::refine_impl(
        DomainDiscretization<vec_t>& domain, const Range<int>& region, int region_idx,
        double fraction, const KDTree<vec_t>& domain_tree, const KDTree<vec_t>& region_tree,
        Range<Range<int>>& children, typename vec_t::scalar_t max_support_radius)  {
    int region_size = region.size();
    int N = domain.size();
    typedef typename vec_t::scalar_t scalar_t;

    assert_msg(region_size > 0, "The region to refine is empty.");

    // Iterate through points in region and generate new points
    int num_new_points = 0;
    for (int i = region_idx; i < region_size; i++) {
        int c = region[i];  // the global domain index
        const vec_t pos = domain.pos(c);
        const Range<int> supp = domain.support(c);
        assert_msg(supp.size() >= 2, "At least 2 nodes must be in support of every node, %d "
                                     "found in support of node %d.", supp.size(), c);
        scalar_t min_dist = fraction * domain.dr(c);

        // then find all the nodes in the radius that admit collisions of children
        scalar_t critical_radius = max_support_radius + min_dist;
        Range<int> critical_idx = region_tree.query(pos, critical_radius * critical_radius).first;
        int n = supp.size();
        // half links to my support
        for (int j = 1; j < n; ++j) {
            int s = supp[j];
            vec_t candidate = 0.5 * (domain.pos(c) + domain.pos(s));

            // decide the type of new node and project to boundary if necessary
            int candidate_type = std::max(domain.type(c), domain.type(s));
            vec_t normal_vector;
            if (candidate_type < 0) {
                normal_vector = domain.normal(c) + domain.normal(s);
                if (normal_vector.squaredNorm() < 1e-15) {
                    // normal_vector of given points point in opposite directions.
                    continue;
                }
                normal_vector.normalize();
                auto result = domain.shape().projectPointToBoundary(candidate, normal_vector);
                if (result.first) {
                    candidate = result.second;
                } else {
                    std::cerr << format("Adding point %s with type %d to the boundary along "
                                        "normal %s was not successful.",
                                        candidate, candidate_type, normal_vector)
                              << std::endl;
                    continue;
                }
            }

            // If nodes are out of the domain, they should be thrown away.
            if (!domain.shape().contains(candidate)) continue;

            // Check that it is not too close to old nodes
            double closest_old_node_dist_squared = domain_tree.query(candidate).second[0];
            bool too_close = closest_old_node_dist_squared < min_dist * min_dist;

            // Check possible collision with new nodes
            // Condition: dist(me, an old node's child) < min_dist
            // Because children are half-links with support, it can only happen if
            // d(p, q) <= max_support_size + min_dist
            if (!too_close) {
                for (int k : critical_idx) {  // check children of k
                    for (int l : children[k]) {
                        if ((domain.pos(l) - candidate).squaredNorm() <
                            min_dist * min_dist) {
                            too_close = true;
                            break;
                        }
                    }
                    if (too_close) break;
                }
            }

            if (!too_close) {  // add new point
                children[i].push_back(N + num_new_points++);  // increase new point counter
                if (candidate_type < 0) {
                    domain.addBoundaryNode(candidate, candidate_type, normal_vector);
                } else {
                    domain.addInternalNode(candidate, candidate_type);
                }
            }
        }
    }

    // return back indices of new nodes
    Range<int> new_ids(num_new_points);
    std::iota(new_ids.begin(), new_ids.end(), N);

    return new_ids;
}

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_HALFLINKSREFINE_HPP_
