#ifndef MEDUSA_BITS_OPERATORS_COMPUTESHAPES_HPP_
#define MEDUSA_BITS_OPERATORS_COMPUTESHAPES_HPP_

/**
 * @file
 * Implementations of shape function computation utilities.
 */

#include "computeShapes_fwd.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <medusa/bits/domains/DomainDiscretization.hpp>
#include <medusa/bits/utils/stdtypesutils.hpp>
#include <Eigen/Core>

namespace mm {

namespace shapes_internal {

/// @cond
template <typename shape_storage_t, typename approx_t>
typename std::enable_if<!tuple_has_type<Lap, typename shape_storage_t::operators_t>::value>::type
computeLapShapes(shape_storage_t*, int, approx_t&) {}

template <typename shape_storage_t, typename approx_t>
typename std::enable_if<(tuple_has_type<Lap, typename shape_storage_t::operators_t>::value) &&
                        (tuple_has_type<Der2s<approx_t::vector_t::dim>,
                                typename shape_storage_t::operators_t>::value)>::type
computeLapShapes(shape_storage_t* storage, int c, approx_t&) {
    Eigen::Matrix<typename shape_storage_t::scalar_t, Eigen::Dynamic, 1> sh = storage->d2(0, 0, c);
    for (int d = 1; d < shape_storage_t::dim; ++d) {
        sh += storage->d2(d, d, c);
    }
    storage->setLaplace(c, sh);
}

template <typename shape_storage_t, typename approx_t>
typename std::enable_if<tuple_has_type<Lap, typename shape_storage_t::operators_t>::value &&
                        !tuple_has_type<Der2s<approx_t::vector_t::dim>,
                                typename shape_storage_t::operators_t>::value>::type
computeLapShapes(shape_storage_t* storage, int c, approx_t& local_approx) {
    std::array<int, shape_storage_t::dim> derivative;
    derivative.fill(0); derivative[0] = 2;
    Eigen::Matrix<typename shape_storage_t::scalar_t, Eigen::Dynamic, 1> sh =
            local_approx.getShape(derivative);
    for (int d = 1; d < shape_storage_t::dim; ++d) {
        derivative[d-1] = 0;
        derivative[d] = 2;
        sh += local_approx.getShape(derivative);
    }
    assert_msg(sh.allFinite(), "Unintended floating point numbers in laplace shape '%s' for node "
                               "%d. Perhaps you have a singular point/basis configuration?", sh, c);
    storage->setLaplace(c, sh);
}

template <typename shape_storage_t, typename approx_t>
typename std::enable_if<!tuple_has_type<Der1s<approx_t::vector_t::dim>,
        typename shape_storage_t::operators_t>::value>::type
computeD1Shapes(shape_storage_t*, int, approx_t&) {}

template <typename shape_storage_t, typename approx_t>
typename std::enable_if<!!tuple_has_type<Der1s<approx_t::vector_t::dim>,
        typename shape_storage_t::operators_t>::value>::type
computeD1Shapes(shape_storage_t* storage, int c, approx_t& local_approx) {
    std::array<int, shape_storage_t::dim> derivative;
    for (int d = 0; d < shape_storage_t::dim; ++d) {
        derivative.fill(0);
        derivative[d] = 1;
        auto sh = local_approx.getShape(derivative);
        assert_msg(sh.allFinite(),
                   "Unintended floating point numbers in D1-%d shape '%s' obtained for node %d. "
                   "Perhaps you have a singular point/basis configuration?", d, sh, c);
        storage->setD1(d, c, sh);
    }
}

template <typename shape_storage_t, typename approx_t>
typename std::enable_if<!tuple_has_type<Der2s<approx_t::vector_t::dim>,
        typename shape_storage_t::operators_t>::value>::type
computeD2Shapes(shape_storage_t*, int, approx_t&) {}

template <typename shape_storage_t, typename approx_t>
typename std::enable_if<!!tuple_has_type<Der2s<approx_t::vector_t::dim>,
        typename shape_storage_t::operators_t>::value>::type
computeD2Shapes(shape_storage_t* storage, int c, approx_t& local_approx) {
    std::array<int, shape_storage_t::dim> derivative;
    for (int d1 = 0; d1 < shape_storage_t::dim; ++d1) {
        for (int d2 = 0; d2 <= d1; ++d2) {
            derivative.fill(0);
            derivative[d1] += 1;
            derivative[d2] += 1;
            auto sh = local_approx.getShape(derivative);
            assert_msg(sh.allFinite(), "Unintended floating point numbers in D2-%d-%d shape "
                                       "'%s' obtained for node %d. Perhaps you have a "
                                       "singular point/basis configuration?",
                       d2, d1, sh, c);
            storage->setD2(d2, d1, c, sh);
        }
    }
}
/// @endcond
}  // namespace shapes_internal

template <sh::shape_flags shape_mask, typename approx_t, typename shape_storage_t>
void computeShapes(const DomainDiscretization<typename approx_t::vector_t>& domain,
                   approx_t approx, const indexes_t& indexes, shape_storage_t* storage) {
    enum { dim = approx_t::vector_t::dim };
    typedef typename approx_t::vector_t vector_t;

    int N = domain.size();
    assert_msg(domain.supports().size() == N,
               "domain.support.size = %d and domain.size = %d, but should be the same. "
               "Did you forget to find support before computing shapes?",
               domain.supports().size(), domain.size());

    for (auto& c : indexes) {
        assert_msg(0 <= c && c < N,
                   "Index %d is not a valid index of a point in the domain, must be in range "
                   "[%d, %d).", c, 0, N);
        assert_msg(!domain.support(c).empty(),
                   "Node %d has empty support! Did you forget to find support before "
                   "computing shapes?", c);
    }

    storage->resize(domain.supportSizes());

    // construct shape functions for every point specified
    int cc;
    int isize = static_cast<int>(indexes.size());
    // create local copies of mls for each thread
    #if !defined(_OPENMP)
    approx_t& local_approx = approx;
    #else
    std::vector<approx_t> approx_copies(omp_get_max_threads(), approx);
    #pragma omp parallel for private(cc) schedule(static)
    #endif
    for (cc = 0; cc < isize; ++cc) {
        int c = indexes[cc];
        //  store local copy of approximation engine -- for parallel computing
        #if defined(_OPENMP)
        approx_t& local_approx = approx_copies[omp_get_thread_num()];
        #endif
        // preps local 1D support domain vector (cache friendly storage)
        int support_size = domain.support(c).size();
        for (int j = 0; j < support_size; ++j) {
            storage->setSupport(c, domain.support(c));
        }
        // resets local mls with local parameters
        Range<vector_t> supp_domain = domain.supportNodes(c);
        local_approx.compute(supp_domain[0], supp_domain);

        shapes_internal::computeD1Shapes(storage, c, local_approx);
        shapes_internal::computeD2Shapes(storage, c, local_approx);
        // must be computed after D2 shapes, as they can potentially be reused
        shapes_internal::computeLapShapes(storage, c, local_approx);
    }
}

/// Shortcut to compute shapes for given domain.
template <class vec_t>
template <sh::shape_flags shape_mask, typename approx_t>
RaggedShapeStorage<vec_t, typename sh::operator_tuple<shape_mask, vec_t::dim>::type>
DomainDiscretization<vec_t>::computeShapes(
        approx_t approx, const indexes_t& indexes) const {
    static_assert(static_cast<int>(vec_t::dim) == static_cast<int>(approx_t::dim),
                  "Domain and approximation engine dimensions do not match");
    RaggedShapeStorage<vec_t, typename sh::operator_tuple<shape_mask, vec_t::dim>::type> storage;
    if (indexes.empty()) mm::computeShapes<shape_mask>(*this, approx, all(), &storage);
    else mm::computeShapes<shape_mask>(*this, approx, indexes, &storage);
    return storage;
}

}  // namespace mm

#endif  // MEDUSA_BITS_OPERATORS_COMPUTESHAPES_HPP_
