#ifndef MEDUSA_BITS_OPERATORS_COMPUTESHAPES_FWD_HPP_
#define MEDUSA_BITS_OPERATORS_COMPUTESHAPES_FWD_HPP_

/**
 * @file
 * Declarations of shape function computation utilities.
 *
 * @example test/operators/computeShapes_test.cpp
 */

#include <medusa/Config.hpp>
#include <medusa/bits/operators/shape_flags.hpp>
#include <iosfwd>

namespace mm {

template <typename vec_t>
class DomainDiscretization;

/**
 * Computes shape functions (stencil weights) for given nodes using support domains from `domain`
 * and approximations provided by `approx`. The computed shapes are stored in `storage`.
 *
 * @tparam shape_mask Which shapes to compute, see mm::sh.
 * @tparam approx_t Type of the approximation engine to use.
 * Must satisfy the @ref approx-concept.
 * @tparam shape_storage_t Type of the shape storage.
 * Must satisfy the @ref ss-concept.
 * @param[in] domain Domain discretization for which to compute the shape functions.
 * @param[in] approx Approximation engine specifying the shape computation procedure.
 * @param[in] indexes A set of indexes for which to compute the shape functions. Common
 * values are @ref DomainDiscretization::all "domain.all()",
 * @ref DomainDiscretization::interior "domain.interior()",
 * @ref DomainDiscretization::boundary "domain.boundary()".
 * @param[out] storage An object for storing the shape functions. The object is filled
 * during computation.
 *
 * @note This function supports parallel execution if OpenMP is enabled in the compiler, e.g.\
 * for GCC: `-fopenmp`, for ICC: `-openmp`.
 *
 * Usage example:
 * @snippet operators/computeShapes_test.cpp computeShapes usage example
 * @ingroup operators
 */
template <sh::shape_flags shape_mask = sh::all, typename approx_t, typename shape_storage_t>
void computeShapes(const DomainDiscretization<typename approx_t::vector_t>& domain,
                   approx_t approx, const indexes_t& indexes, shape_storage_t* storage);

}  // namespace mm

#endif  // MEDUSA_BITS_OPERATORS_COMPUTESHAPES_FWD_HPP_
