#ifndef MEDUSA_BITS_APPROXIMATIONS_OPERATORS_FWD_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_OPERATORS_FWD_HPP_

/**
 * @file
 * File defining basic differential operator families.
 */

#include <medusa/Config.hpp>
#include <array>
#include <vector>
#include <Eigen/Core>

namespace mm {

/**
 * Base class for a differential operator. Also doubles as a one-element family.
 * @tparam Derived Derived class inheriting, used for CRTP.
 */
template <typename Derived>
struct Operator {
    static int size() { return 1; }   ///< Size is always 1.
    static std::string name() { return Derived::name(); }   ///< CRTP for human-readable name.
};

/// Represents the Laplacian operator
struct Lap : public Operator<Lap> {
    static std::string name() { return "Lap"; }  ///< Human-readable name.
};

/**
 * Represents a family of all first derivatives.
 * @tparam Dimension of the space
 */
template <int dimension>
struct Der1s {
    enum { dim = dimension };
    static int size() { return dim * (dim + 1) / 2; }  ///< Size of this family is `dim`.
    static std::string name() { return "Der1s"; }  ///< Human-readable name.
};


/**
 * Represents a family of all second derivatives.
 * @tparam Dimension of the space
 */
template <int dimension>
struct Der2s {
    enum { dim = dimension };
    static int size() { return dim*(dim+1)/2; }  ///< Size of this family is `dim*(dim-1)/2`.
    static std::string name() { return "Der2s"; }  ///< Human-readable name.
};

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_OPERATORS_FWD_HPP_
