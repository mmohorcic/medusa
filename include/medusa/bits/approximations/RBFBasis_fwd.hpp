#ifndef MEDUSA_BITS_APPROXIMATIONS_RBFBASIS_FWD_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_RBFBASIS_FWD_HPP_

/**
 * @file
 * Declaration of RBF basis.
 *
 * @example test/approximations/RBFBasis_test.cpp
 */

#include <medusa/Config.hpp>
#include <iosfwd>
#include <array>

namespace mm {

/**
 * Represents a basis of Radial Basis Functions over a local neighbourhood.
 * @tparam RBFType Type of the RBF used. Must satisfy the @ref rbf-concept.
 * @tparam vec_t Vector type used in calculations.
 *
 * This class satisfies the @ref basis-concept.
 *
 * Usage example:
 * @snippet approximations/RBFBasis_test.cpp RBF basis usage example
 * @ingroup approximations
 */
template <class RBFType, class vec_t>
class RBFBasis {
    static_assert(std::is_same<typename vec_t::scalar_t, typename RBFType::scalar_t>::value,
                  "Basis and underlying RBF must have the same scalar type.");
  public:
    typedef RBFType rbf_t;   ///< Radial basis function type.
    typedef vec_t vector_t;   ///< Vector type.
    typedef typename vector_t::scalar_t scalar_t;   ///< Scalar type.
    /// Store dimension of the domain.
    enum { /** Dimensionality of the function domain. */ dim = vec_t::dim };
    typedef std::array<int, vector_t::dim> derivative_t;  ///< Type for specifying derivatives.

  private:
    int size_;  ///< Basis size.
    rbf_t rbf_;  ///< RBF function.

  public:
    /// Construct a basis of `basis_size` with default construction of RBF.
    explicit RBFBasis(int basis_size) : size_(basis_size), rbf_() {}
    /// Construct a basis of `basis_size` with given RBF.
    RBFBasis(int basis_size, const rbf_t& rbf) : size_(basis_size), rbf_(rbf) {}

    /// Returns basis size.
    int size() const { return size_; }

    /// Returns underlying RBF object.
    const rbf_t& rbf() const { return rbf_; }

    /// Returns modifiable underlying RBF object.
    rbf_t& rbf() { return rbf_; }

    /**
     * Evaluates `index`-th RBF's derivative at `point`.
     *
     * @param index A number in `[0, size())` specifying the index of RBF to evaluate.
     * @param point Point in which to evaluate the RBF.
     * @param support Points in local neighbourhood used as centers for RBFs.
     * @param derivative The description of which derivative you want. For example `{1, 1, 3}`
     * represents @f$\frac{d^5}{dx\, dy\, dz^3}@f$
     * @return Value of the derivative of requested monomial at given point.
     * @throws Assertion fails if `index` is out of range or if an invalid derivative is requested.
     *
     * @note Derivatives only up to combined order of 2 are currently supported.
     */
    scalar_t operator()(int index, const vector_t& point, const derivative_t& derivative,
                        const std::vector<vector_t>& support) const;

    /// Evaluate `index`-th function at `point`.
    scalar_t operator()(int index, const vector_t& point,
                        const std::vector<vector_t>& support) const;

    /// Evaluate `index`-th function at zero.
    scalar_t evalAt0(int index, const std::vector<vector_t>& support) const;

    /// Evaluate `index`-th function derivative at zero.
    scalar_t evalAt0(int index, const derivative_t& derivative,
                     const std::vector<vector_t>& support) const;

    /// Output basic info about given basis.
    template <typename V, typename R>
    friend std::ostream& operator<<(std::ostream& os, const RBFBasis<V, R>& m);
};

// Convenience typedefs

template <typename S> class Gaussian;
/// RBF basis using Gaussian RBF. Defined for convenience.
template <typename V> using Gaussians = RBFBasis<Gaussian<typename V::scalar_t>, V>;

template <typename S> class Multiquadric;
/// RBF basis using Multiquadric RBF. Defined for convenience.
template <typename V> using MQs = RBFBasis<Multiquadric<typename V::scalar_t>, V>;

template <typename S> class InverseMultiquadric;
/// RBF basis using InverseMultiquadric RBF. Defined for convenience.
template <typename V> using IMQs = RBFBasis<InverseMultiquadric<typename V::scalar_t>, V>;

template <typename S, int K> class Polyharmonic;
/// RBF basis using Polyharmonic RBF. Defined for convenience.
template <typename V, int K = -1> using PHs = RBFBasis<Polyharmonic<typename V::scalar_t, K>, V>;

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_RBFBASIS_FWD_HPP_
