#ifndef MEDUSA_BITS_APPROXIMATIONS_RBFFD_FWD_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_RBFFD_FWD_HPP_

/**
 * @file
 * Declaration of Radial Basis Function Finite Difference approximation.
 *
 * @example test/approximations/RBFFD_test.cpp
 */

#include <medusa/Config.hpp>
#include <medusa/bits/types/Range_fwd.hpp>
#include <Eigen/Core>
#include "RBFBasis_fwd.hpp"
#include "Monomials_fwd.hpp"
#include "ScaleFunction.hpp"

namespace mm {

/**
 * Computes a RBF-FD approximation of given operator over local neighbourhood.
 *
 * @tparam RBFType Type of RBF used in approximation. Must satisfy the @ref rbf-concept.
 * @tparam vec_t Vector type used in calculations.
 * @tparam scale_t Scale function to be used in approximation. Must satisfy the @ref scale-concept.
 * @tparam solver_t Which linear solver to use in weight computations. Must satisfy the
 * @ref linsolve-concept. Default: LDL^T.
 *
 * This class satisfies the @ref approx-concept.
 *
 * Usage example:
 * @snippet approximations/RBFFD_test.cpp RBFFD usage example
 * @ingroup approximations
 */
template <class RBFType, class vec_t, class scale_t = NoScale, class solver_t =
    Eigen::PartialPivLU<Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>
class RBFFD {
    static_assert(std::is_same<typename vec_t::scalar_t, typename RBFType::scalar_t>::value,
                  "Basis and underlying RBF must have the same scalar type.");
  public:
    typedef RBFType rbf_t;   ///< Radial basis function type.
    typedef vec_t vector_t;   ///< Vector type.
    typedef typename vector_t::scalar_t scalar_t;   ///< Scalar type.
    /// Store dimension of the domain.
    enum { /** Dimensionality of the function domain. */ dim = vec_t::dim };
    typedef std::array<int, vector_t::dim> derivative_t;  ///< Type for specifying derivatives.

  private:
    rbf_t rbf_;  ///< RBF used in the approximation.
    Monomials<vec_t> mon_;  ///< Monomials used in the approximation.
    solver_t solver_;  ///< Linear solver used in the approximation.

    vector_t point_;  ///< Saved point.
    scalar_t scale_;  ///< Saved scale.
    Range<vec_t> support_;  ///< Saved support scaled to local coordinate system.

  public:
    /// Construct a RBFFD engine from given RBF using no monomial augmentation.
    explicit RBFFD(rbf_t rbf) : rbf_(rbf), mon_(), point_(NaN) {}

    /// Construct a RBFFD engine from given RBF with additional monomial augmentation.
    RBFFD(rbf_t rbf, const Monomials<vec_t>& mon) : rbf_(rbf), mon_(mon), point_(NaN) {}

    /// Return RBF used in this approximation.
    const rbf_t& rbf() const { return rbf_; }

    /// Returns minimal support size required for this approximation to be performed.
    int requiredSupportSize() const { return mon_.size(); }

    /**
     * Setup this engine to approximate operators at given point over given local neighbourhood.
     * @param point At which point to construct the approximation.
     * @param support Coordinates of local support points.
     */
    void compute(const vector_t& point, const std::vector<vector_t>& support);

    /// Return shape function (stencil weights) for value approximation.
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> getShape() const;

    /// Return shape function (stencil weights) for given derivative operator.
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> getShape(const derivative_t& der) const;

    /// Output basic info about given approximation engine.
    template <class R, class V, class S, class L>
    friend std::ostream& operator<<(std::ostream& os, const RBFFD<R, V, S, L>& e);
};

/// Output basic info about given approximation engine.
template <class R, class V, class S, class L>
std::ostream& operator<<(std::ostream& os, const RBFFD<R, V, S, L>& e) {
    os << "RBFFD approximation engine:\n"
       << "    dimension: " << e.dim << '\n';
    if (e.point_[0] != e.point_[0]) {
        os << "    last point: not used yet\n";
    } else {
        os << "    last point: " << e.point_ << '\n';
    }
    os << "    RBF: " << e.rbf_ << '\n'
       << "    Augmentation: " << e.mon_ << '\n'
       << "    scale: " << S();
    return os;
}

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_RBFFD_FWD_HPP_
