#ifndef MEDUSA_BITS_APPROXIMATIONS_RBFFD_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_RBFFD_HPP_

/**
 * @file
 * Implementation of Radial Basis Function Finite Difference approximation.
 */

#include "RBFFD_fwd.hpp"
#include <vector>
#include <medusa/bits/utils/numutils.hpp>
#include <Eigen/LU>

namespace mm {

/// @cond
template <class RBFType, class vec_t, class scale_t, class solver_t>
void RBFFD<RBFType, vec_t, scale_t, solver_t>::compute(
        const vec_t& point, const std::vector<vec_t>& support) {
    scale_ = scale_t::scale(point, support);
    int n1 = support.size();
    assert_msg(n1 > 0, "Cannot construct a RBFFD approximation with no stencil nodes. "
                       "Did you forget to cal findSupport?.");
    int n2 = mon_.size();
    int N = n1 + n2;
    Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic> M(N, N);
    // RBF part
    for (int i = 0; i < n1; ++i) {
        for (int j = 0; j < i; ++j) {
            M(i, j) = M(j, i) = rbf_(((support[i]-support[j])/scale_).squaredNorm());
        }
        M(i, i) = rbf_(0);
    }

    // Local scaled support
    support_.resize(n1);
    for (int i = 0; i < n1; ++i) {
        support_[i] = (support[i] - point) / scale_;
    }

    // Monomial part
    for (int i = 0; i < n2; ++i) {
        for (int j = 0; j < n1; ++j) {
            M(n1+i, j) = M(j, n1+i) = mon_(i, support_[j], support_);
        }
    }
    M.bottomRightCorner(n2, n2).setZero();

    point_ = point;
    solver_.compute(M);
}

template <class RBFType, class vec_t, class scale_t, class solver_t>
Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, 1>
RBFFD<RBFType, vec_t, scale_t, solver_t>::getShape() const {
    int n = support_.size();
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> rhs(n + mon_.size());
    for (int i = 0; i < n; ++i) {
        rhs(i) = rbf_(support_[i].squaredNorm());
    }
    for (int i = 0; i < mon_.size(); ++i) {
        rhs(n+i) = mon_.evalAt0(i, support_);
    }
    return solver_.solve(rhs).head(n);
}

template <class RBFType, class vec_t, class scale_t, class solver_t>
Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, 1>
RBFFD<RBFType, vec_t, scale_t, solver_t>::getShape(
        const RBFFD<RBFType, vec_t, scale_t, solver_t>::derivative_t& der) const {
    int n = support_.size();
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> rhs(n + mon_.size());
    RBFBasis<RBFType, vec_t> rbf_basis(n, rbf_);
    for (int i = 0; i < n; ++i) {
        rhs(i) = rbf_basis.evalAt0(i, der, support_);
    }
    for (int i = 0; i < mon_.size(); ++i) {
        rhs(n+i) = mon_.evalAt0(i, der, support_);
    }
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> result = solver_.solve(rhs).head(n);
    int der_order = 0;
    for (int i = 0; i < dim; ++i) { der_order += der[i]; }
    return result / ipow(scale_, der_order);
}
/// @endcond

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_RBFFD_HPP_
