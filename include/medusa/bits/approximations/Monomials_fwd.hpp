#ifndef MEDUSA_BITS_APPROXIMATIONS_MONOMIALS_FWD_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_MONOMIALS_FWD_HPP_

/**
 * @file
 * Declaration of Monomial basis.
 *
 * @example test/approximations/Monomials_test.cpp
 */

#include <medusa/Config.hpp>
#include <cmath>
#include <array>
#include <Eigen/Core>
#include <iosfwd>

namespace mm {

/**
 * A class representing Monomial basis.
 *
 * This class satisfies the @ref basis-concept.
 *
 * Usage example:
 * @snippet approximations/Monomials_test.cpp Monomials usage example
 * @ingroup approximations
 */
template <class vec_t>
class Monomials {
  public:
    typedef vec_t vector_t;   ///< Vector type.
    typedef typename vector_t::scalar_t scalar_t;   ///< Scalar type.
    /// Store dimension of the domain.
    enum { /** Dimensionality of the function domain. */ dim = vec_t::dim };
    typedef std::array<int, vector_t::dim> derivative_t;  ///< Type for specifying derivatives.

  private:
    /// A vector describing monomials with the powers of every coordinate
    Eigen::Matrix<int, dim, Eigen::Dynamic> powers_;

    /// Constructs basis from a vector of powers.
    void setFromPowers(const std::vector<std::vector<int>>& powers);

    /// Generate powers for `dim`-d monomials up to `max_order`.
    static std::vector<std::vector<int>> generatePowers(int max_order, int dim);

  public:
    /// Construct empty monomial basis with `size = 0`.
    Monomials() = default;

    /**
     * Construct a basis of all monomials of combined order lower or equal to `order`.
     * @param order Maximal combined order of monomials to be used.
     *  If `order` is `-1` then empty basis is constructed.
     *
     * Example: If you call this with an `order = 2` parameter and Vec2d template parameter,
     * your basis will consist of @f$ \{1, x, y, x^2, xy, y^2\} @f$ (not necessarily in that order).
     */
    Monomials(int order);

    /**
     * @brief Construct monomial basis from monomials with specific powers.
     *
     * @param powers List of lists of size `dim`, each representing powers of a monomial.
     * For example `{{1, 2}, {0, 3}, {2, 0}}` in 2D represents monomials @f$\{x y^2, y^3, x^2\}@f$.
     */
    Monomials(const std::vector<std::vector<int>>& powers) { setFromPowers(powers); }

    /**
     * Construct a tensor basis of monomials ranging from 0 up to `order` (inclusive) in each
     * dimension. If `order` is `-1` then empty basis is constructed.
     *
     * Example: `tensorBasis(2)` in 2D constructs the set
     * @f$\{1, x, x^2, y, yx, yx^2, y^2, y^2x, y^2x^2\}@f$.
     */
    static Monomials<vec_t> tensorBasis(int order);

    /// Return number of monomials in this basis.
    int size() const { return powers_.cols(); }

    /// Get saved monomial powers.
    const Eigen::Matrix<int, dim, Eigen::Dynamic>& powers() const { return powers_; }

    /**
     * Evaluates `index`-th monomial's derivative at `point`.
     *
     * @param index A number in `[0, size())` specifying the index of a monomial to evaluate.
     * @param point Point in which to evaluate the monomial.
     * @param derivative The description of which derivative you want. For example `{1, 1, 3}`
     * represents @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return Value of the derivative of requested monomial at given point.
     * @throws Assertion fails if `index` is out of range or if an invalid derivative is requested.
     */
    scalar_t operator()(int index, const vec_t& point, const derivative_t& derivative,
                        const std::vector<vector_t>& /* support */ = {}) const;

    /// Evaluate `index`-th monomial at `point`.
    scalar_t operator()(int index, const vector_t& point,
                        const std::vector<vector_t>& /* support */ = {}) const;

    /// Evaluate `index`-th monomial at zero.
    scalar_t evalAt0(int index, const std::vector<vector_t>& /* support */ = {}) const;

    /// Evaluate `index`-th monomial derivative at zero.
    scalar_t evalAt0(int index, const derivative_t& derivative,
                     const std::vector<vector_t>& /* support */ = {}) const;

    /// Output basic info about given Monomial basis.
    template <class V>
    friend std::ostream& operator<<(std::ostream& os, const Monomials<V>& m);
};

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_MONOMIALS_FWD_HPP_
