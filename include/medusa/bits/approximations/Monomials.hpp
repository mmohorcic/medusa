#ifndef MEDUSA_BITS_APPROXIMATIONS_MONOMIALS_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_MONOMIALS_HPP_

/**
 * @file
 * Implementation of Monomial basis.
 */

#include "Monomials_fwd.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <medusa/bits/utils/numutils.hpp>
#include <iostream>

namespace mm {

template <class vec_t>
Monomials<vec_t>::Monomials(int order)  {
    assert_msg(-1 <= order, "Requested monomials of negative order %d.", order);
    setFromPowers(generatePowers(order, dim));
}

template <class vec_t>
std::vector<std::vector<int>> Monomials<vec_t>::generatePowers(int max_order, int dim) {
    if (max_order < 0) return {};
    if (dim == 0) return {{}};
    std::vector<std::vector<int>> powers;
    for (int i = 0; i <= max_order; ++i) {
        auto other = generatePowers(max_order - i, dim-1);
        for (const auto& p : other) {
            powers.push_back({i});
            for (int o : p) {
                powers.back().push_back(o);
            }
        }
    }
    return powers;
}

template <class vec_t>
Monomials<vec_t> Monomials<vec_t>::tensorBasis(int order) {
    assert_msg(-1 <= order, "Requested monomials of negative order %d.", order);
    if (order == -1) return Monomials<vec_t>();
    std::vector<std::vector<int>> powers;
    Vec<int, dim> counter = 0;
    Vec<int, dim> counts = order+1;
    do {
        powers.push_back(std::vector<int>(counter.begin(), counter.end()));
    } while (incrementCounter(counter, counts));
    return Monomials<vec_t>(powers);
}

template <class vec_t>
void Monomials<vec_t>::setFromPowers(const std::vector<std::vector<int>>& powers) {
    powers_.resize(dim, powers.size());
    int size = powers.size();
    for (int i = 0; i < size; ++i) {
        const std::vector<int>& p = powers[i];
        assert_msg(p.size() == dim, "Monomial size %s does not match dimension %d", p.size(), dim);
        for (int j = 0; j < dim; ++j) {
            powers_(j, i) = p[j];
        }
    }
}

/// @cond
template <class vec_t>
typename vec_t::scalar_t Monomials<vec_t>::operator()(
        int index, const vec_t& point, const std::vector<vector_t>& /* support */) const {
    assert_msg(0 <= index && index < size(), "Monomial at index %d does not exist. "
            "Index must be in range [0, %d).", index, size());
    scalar_t result(1.0);
    for (int i = 0; i < dim; i++) {
        result *= ipow(point[i], powers_(i, index));
    }
    return result;
}

template <class vec_t>
typename vec_t::scalar_t Monomials<vec_t>::operator()(
        int index, const vec_t& point, const derivative_t& derivative,
        const std::vector<vector_t>&) const  {
    assert_msg(0 <= index && index < size(), "Monomial at index %d does not exist. "
            "Index must be in range [0, %d).", index, size());
    for (int x : derivative) {
        assert_msg(x >= 0, "Derivative of negative order %d requested.", x);
    }
    scalar_t result(1.0);
    for (int i = 0; i < dim; i++) {
        if (derivative[i] > powers_(i, index)) {
            result = 0;
            break;
        }
        result *= ipow(point[i], powers_(i, index) - derivative[i]);
        for (int j = powers_(i, index); j > powers_(i, index) - derivative[i]; j--) {
            result *= j;
        }
    }
    return result;
}
/// @endcond

/// Output basic info about given Monomial basis.
template <class V>
std::ostream& operator<<(std::ostream& os, const Monomials<V>& m) {
    return os << "Monomials " << m.dim << "D: "
              << m.powers_.transpose() << ", number of functions = " << m.size();
}

template <class vec_t>
typename vec_t::scalar_t Monomials<vec_t>::evalAt0(int index, const derivative_t& derivative,
                                                   const std::vector<vector_t>&) const {
    assert_msg(0 <= index && index < size(), "Monomial at index %d does not exist. "
                                             "Index must be in range [0, %d).", index, size());
    for (int x : derivative) {
        assert_msg(x >= 0, "Derivative of negative order %d requested.", x);
    }
    scalar_t r = 1;
    for (int i = 0; i < dim; ++i) {
        if (powers_(i, index) == derivative[i]) {
            for (int j = 2; j <= powers_(i, index); ++j) {
                r *= j;
            }
        } else {
            return 0;
        }
    }
    return r;
}

template <class vec_t>
typename vec_t::scalar_t Monomials<vec_t>::evalAt0(int index, const std::vector<vector_t>&) const {
    assert_msg(0 <= index && index < size(), "Monomial at index %d does not exist. "
                                             "Index must be in range [0, %d).", index, size());
    return powers_.col(index).sum() == 0;
}

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_MONOMIALS_HPP_
