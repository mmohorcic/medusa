#include <medusa/bits/types/Vec.hpp>
#include <medusa/bits/domains/BoxShape.hpp>
#include <medusa/bits/utils/stdtypesutils.hpp>
#include <map>
#include "medusa/bits/domains/FindClosest.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(DomainEngines, FindClosestAll) {
    BoxShape<Vec2d> box({0, 0}, {1, 1});
    DomainDiscretization<Vec2d> d = box.discretizeWithStep(0.1);
    int N = d.size();
    Range<int> expected(N);
    for (int i = 0; i < N; ++i) expected[i] = i;
    d.findSupport(FindClosest(N));  // should get everything
    for (auto x : d.supports()) {
        sort(x);
        ASSERT_EQ(expected.size(), x.size());
        EXPECT_EQ(expected, x);
    }
}

// find in radius
/*
// see img/test_case_domain_support.png
TEST(DomainEngines, FindClosestNoLimit) {
    BoxShape<Vec2d> box({0, 0}, {3, 3});
    DomainDiscretization<Vec2d> d = box.discretizeWithStep(1.0);
    d.findSupport(2, 10);  // should not be limiting
    int num = d.size();
    std::map<Vec2d, Range<Vec2d>> expected({
       {{0, 0}, {{0, 0}, {0, 1}, {1, 0}, {1, 1}}},
       {{1, 0}, {{0, 0}, {0, 1}, {1, 0}, {1, 1}, {2, 0}, {2, 1}}},
       {{2, 0}, {{1, 0}, {1, 1}, {2, 0}, {2, 1}, {3, 0}, {3, 1}}},
       {{3, 0}, {{2, 0}, {2, 1}, {3, 0}, {3, 1}}},
       {{0, 1}, {{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}}},
       {{1, 1}, {{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}}},
       {{2, 1}, {{1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}, {3, 0}, {3, 1}, {3, 2}}},
       {{3, 1}, {{2, 0}, {2, 1}, {2, 2}, {3, 0}, {3, 1}, {3, 2}}},
       {{0, 2}, {{0, 1}, {0, 2}, {0, 3}, {1, 1}, {1, 2}, {1, 3}}},
       {{1, 2}, {{0, 1}, {0, 2}, {0, 3}, {1, 1}, {1, 2}, {1, 3}, {2, 1}, {2, 2}, {2, 3}}},
       {{2, 2}, {{1, 1}, {1, 2}, {1, 3}, {2, 1}, {2, 2}, {2, 3}, {3, 1}, {3, 2}, {3, 3}}},
       {{3, 2}, {{2, 1}, {2, 2}, {2, 3}, {3, 1}, {3, 2}, {3, 3}}},
       {{0, 3}, {{0, 2}, {0, 3}, {1, 2}, {1, 3}}},
       {{1, 3}, {{0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 2}, {2, 3}}},
       {{2, 3}, {{1, 2}, {1, 3}, {2, 2}, {2, 3}, {3, 2}, {3, 3}}},
       {{3, 3}, {{2, 2}, {2, 3}, {3, 2}, {3, 3}}},
    });
    for (int i = 0; i < num; ++i) {
        Vec2d cur = d.pos(i);
        Range<Vec2d> a = d.supportNodes(i);
        sort(a.begin(), a.end());
        EXPECT_EQ(expected[d.pos(i)], a);
    }
} */


// see img/test_case_domain_support.png
TEST(DomainEngines, FindClosest2d) {
    BoxShape<Vec2d> box({0, 0}, {3, 3});
    DomainDiscretization<Vec2d> d = box.discretizeWithStep(1.0);
    int ss = 5;
    d.findSupport(FindClosest(ss));
    std::map<Vec2d, Range<Vec2d>> expected({
       {{1, 1}, {{0, 1}, {1, 0}, {1, 1}, {1, 2}, {2, 1}}},
       {{2, 1}, {{1, 1}, {2, 0}, {2, 1}, {2, 2}, {3, 1}}},
       {{1, 2}, {{0, 2}, {1, 1}, {1, 2}, {1, 3}, {2, 2}}},
       {{2, 2}, {{1, 2}, {2, 1}, {2, 2}, {2, 3}, {3, 2}}},
    });
    for (const auto& x : d.supports()) {
        EXPECT_LE(x.size(), ss);
    }
    for (int i : d.interior()) {  // internals are well defined
        Vec2d cur = d.pos(i);
        Range<Vec2d> a = d.supportNodes(i);
        sort(a.begin(), a.end());
        EXPECT_EQ(expected[cur], a);
    }
}

TEST(DomainEngines, FindClosestForWhich) {
    BoxShape<Vec1d> box(0, 1);
    DomainDiscretization<Vec1d> d(box);
    Range<Vec1d> pos = {1., 2., 3., 4., 5., 6., 7.};
    Range<int> types = {-1, 2, -1, 2, 2, -1, -1};

    for (int i = 0; i < pos.size(); ++i) {
        if (types[i] > 0) d.addInternalNode(pos[i], types[i]);
        else d.addBoundaryNode(pos[i], types[i], pos[i]);
    }

    FindClosest f(1); f.forNodes(d.boundary());
    d.findSupport(f);
    Range<Range<int>> expected = {{0}, {}, {2}, {}, {}, {5}, {6}};
    EXPECT_EQ(expected, d.supports());
}

TEST(DomainEngines, FindClosestSearchAmong) {
    BoxShape<Vec1d> box(0, 1);
    DomainDiscretization<Vec1d> d(box);
    Range<Vec1d> pos = {1., 2., 3., 4., 5., 6., 7.};
    Range<int> types = {-1, 2, -1, 2, 2, -1, -1};
    for (int i = 0; i < pos.size(); ++i) {
        if (types[i] > 0) d.addInternalNode(pos[i], types[i]);
        else d.addBoundaryNode(pos[i], types[i], pos[i]);
    }

    FindClosest f(1); f.searchAmong(d.interior());
    d.findSupport(f);
    Range<Range<int>> expected = {{1}, {1}, {1}, {3}, {4}, {4}, {4}};
    EXPECT_EQ(expected, d.supports());
}

TEST(DomainEngines, FindClosestForceSelf) {
    BoxShape<Vec1d> box(0, 1);
    DomainDiscretization<Vec1d> d(box);
    Range<Vec1d> pos = {1., 2., 3., 4., 5., 6., 7.};
    Range<int> types = {-1, 2, -1, 2, 2, -1, -1};
    for (int i = 0; i < pos.size(); ++i) {
        if (types[i] > 0) d.addInternalNode(pos[i], types[i]);
        else d.addBoundaryNode(pos[i], types[i], pos[i]);
    }

    /// [FindClosest usage example]
    // Find the closest interior node for all nodes.
    FindClosest f(1); f.searchAmong(d.interior()).forceSelf(false);
    d.findSupport(f);  // d is some DomainDiscretization
    /// [FindClosest usage example]
    Range<Range<int>> expected = {{1}, {1}, {1}, {3}, {4}, {4}, {4}};
    EXPECT_EQ(expected, d.supports());
    f.forceSelf();
    d.findSupport(f);
    expected = {{0}, {1}, {2}, {3}, {4}, {5}, {6}};
    EXPECT_EQ(expected, d.supports());
    f.numClosest(2);
    d.findSupport(f);
    expected = {{0, 1}, {1, 3}, {2, 1}, {3, 4}, {4, 3}, {5, 4}, {6, 4}};
    EXPECT_EQ(expected, d.supports());
}

TEST(DomainEngines, FindClosestDeath) {
    BoxShape<Vec2d> box({0, 0}, {1, 1});
    DomainDiscretization<Vec2d> d = box.discretizeWithStep(0.1);

    FindClosest f(-2);
    EXPECT_DEATH(d.findSupport(f), "Support size must be greater than 0");
    f.numClosest(122);
    EXPECT_DEATH(d.findSupport(f), "Support size \\(122) cannot exceed number of points that we "
                                   "are searching among \\(121).");
    f.numClosest(5).forNodes({1000});
    EXPECT_DEATH(d.findSupport(f), "Index 1000 out of range \\[0, 121) in forNodes.");
    f.numClosest(1).forNodes({}).searchAmong({5000});
    EXPECT_DEATH(d.findSupport(f), "Index 5000 out of range \\[0, 121) in searchAmong.");
}

}  // namespace mm
