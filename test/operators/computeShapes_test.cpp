#include <medusa/bits/operators/computeShapes.hpp>
#include <medusa/bits/operators/UniformShapeStorage.hpp>

#include <medusa/bits/domains/FindClosest.hpp>
#include <medusa/bits/operators/RaggedShapeStorage.hpp>
#include <medusa/bits/domains/DomainDiscretization.hpp>
#include <medusa/bits/domains/BoxShape.hpp>
#include <medusa/bits/approximations/WLS_fwd.hpp>
#include <medusa/bits/approximations/WeightFunction.hpp>
#include <medusa/bits/approximations/JacobiSVDWrapper_fwd.hpp>
#include <medusa/bits/approximations/Monomials.hpp>

#include "gtest/gtest.h"

namespace mm {

TEST(Operators, computeShapes) {
    /// [computeShapes usage example]
    BoxShape<Vec2d> box(0.0, 1.0);
    DomainDiscretization<Vec2d> d = box.discretizeWithStep(0.1);
    d.findSupport(FindClosest(9));
    WLS<Monomials<Vec2d>> approx(2);
    auto shapes = d.computeShapes<sh::lap|sh::d1>(approx);  // implicit call via domain hook

    // direct call which overwrites shapes for Laplacian for internal nodes in given storage
    computeShapes<sh::lap>(d, approx, d.interior(), &shapes);
    /// [computeShapes usage example]

    int node = 12;
    approx.compute(d.pos(node), d.supportNodes(node));
    Eigen::VectorXd sh = approx.getShape({1, 0});  // d/dx shape
    Eigen::VectorXd sh2 = shapes.d1(0, node);
    EXPECT_EQ(sh, sh2);

    node = 5;
    approx.compute(d.pos(node), d.supportNodes(node));
    sh = approx.getShape({0, 1});  // d/dy shape
    sh2 = shapes.d1(1, node);
    EXPECT_EQ(sh, sh2);

    node = 34;
    approx.compute(d.pos(node), d.supportNodes(node));
    sh = approx.getShape({2, 0}) + approx.getShape({0, 2});  // lap shape
    sh2 = shapes.laplace(node);
    EXPECT_EQ(sh, sh2);
}

TEST(Operators, computeShapesD2) {
    BoxShape<Vec2d> box(0.0, 1.0);
    DomainDiscretization<Vec2d> d = box.discretizeWithStep(0.1);
    d.findSupport(FindClosest(9));
    WLS<Monomials<Vec2d>> approx(2);
    auto shapes = d.computeShapes<sh::lap|sh::d2>(approx);

    int node = 22;
    approx.compute(d.pos(node), d.supportNodes(node));
    Eigen::VectorXd sh = approx.getShape({2, 0});  // d/dx^2 shape
    Eigen::VectorXd sh2 = shapes.d2(0, 0, node);
    EXPECT_EQ(sh, sh2);

    node = 0;
    approx.compute(d.pos(node), d.supportNodes(node));
    sh = approx.getShape({0, 2});  // d/dy^2 shape
    sh2 = shapes.d2(1, 1, node);
    EXPECT_EQ(sh, sh2);

    node = 46;
    approx.compute(d.pos(node), d.supportNodes(node));
    sh = approx.getShape({1, 1});  // d/dy^2 shape
    sh2 = shapes.d2(0, 1, node);
    EXPECT_EQ(sh, sh2);

    node = 78;
    approx.compute(d.pos(node), d.supportNodes(node));
    sh = approx.getShape({2, 0}) + approx.getShape({0, 2});  // lap shape
    sh2 = shapes.laplace(node);
    EXPECT_EQ(sh, sh2);
}

TEST(Operators, computeShapesEmptySupport) {
    BoxShape<Vec2d> box(0.0, 1.0);
    DomainDiscretization<Vec2d> d = box.discretizeWithStep(0.1);
    WLS<Monomials<Vec2d>> approx(2);
    UniformShapeStorage<Vec2d, std::tuple<Lap>> storage;
    EXPECT_DEATH(computeShapes<sh::lap>(d, approx, d.interior(), &storage),
            "Did you forget to find support before computing shapes?");
}

}  // namespace mm
