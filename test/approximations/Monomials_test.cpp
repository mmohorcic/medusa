#include <medusa/bits/approximations/Monomials.hpp>

#include "gtest/gtest.h"

namespace mm {

TEST(Approximations, MonomialsTensorBaiss) {
    Monomials<Vec2d> basis = Monomials<Vec2d>::tensorBasis(2);

    Vec2d p = {0.3, 2.3};
    // Values
    EXPECT_DOUBLE_EQ(1, basis(0, p));
    EXPECT_DOUBLE_EQ(p[1], basis(1, p));
    EXPECT_DOUBLE_EQ(p[1] * p[1], basis(2, p));
    EXPECT_DOUBLE_EQ(p[0], basis(3, p));
    EXPECT_DOUBLE_EQ(p[0] * p[1], basis(4, p));
    EXPECT_DOUBLE_EQ(p[0] * p[1] * p[1], basis(5, p));
    EXPECT_DOUBLE_EQ(p[0] * p[0], basis(6, p));
    EXPECT_DOUBLE_EQ(p[0] * p[0] * p[1], basis(7, p));
    EXPECT_DOUBLE_EQ(p[0] * p[0] * p[1] * p[1], basis(8, p));
}

TEST(Approximations, Monomials1D) {
    Monomials<Vec1d> basis(9);
    EXPECT_EQ(10, basis.size());

    Range<Vec1d> points = linspace(Vec1d(-1.0), {1.0}, 200);
    for (auto& p : points) {
        // Values
        EXPECT_DOUBLE_EQ(1, basis(0, p));
        EXPECT_DOUBLE_EQ(p[0], basis(1, p));
        EXPECT_DOUBLE_EQ(p[0] * p[0], basis(2, p));
        // First derivatives
        EXPECT_DOUBLE_EQ(0, basis(0, p, {1}, {}));
        EXPECT_DOUBLE_EQ(1, basis(1, p, {1}, {}));
        EXPECT_DOUBLE_EQ(2 * p[0], basis(2, p, {1}, {}));
        // Second derivatives
        EXPECT_DOUBLE_EQ(0, basis(0, p, {2}, {}));
        EXPECT_DOUBLE_EQ(0, basis(1, p, {2}, {}));
        EXPECT_DOUBLE_EQ(2, basis(2, p, {2}, {}));
    }
}

TEST(Approximations, Monomials2DAtZero) {
    Monomials<Vec2d> basis({{2, 3}, {0, 0}, {1, 2}, {1, 0}});
    EXPECT_EQ(0, basis.evalAt0(0));
    EXPECT_EQ(1, basis.evalAt0(1));
    EXPECT_EQ(0, basis.evalAt0(2));
    EXPECT_EQ(0, basis.evalAt0(3));

    EXPECT_EQ(2*3*2, basis.evalAt0(0, {2, 3}, {}));
    EXPECT_EQ(0, basis.evalAt0(1, {1, 0}, {}));
    EXPECT_EQ(0, basis.evalAt0(2, {2, 2}, {}));
    EXPECT_EQ(0, basis.evalAt0(2, {0, 2}, {}));
    EXPECT_EQ(2, basis.evalAt0(2, {1, 2}, {}));
    EXPECT_EQ(1, basis.evalAt0(3, {1, 0}, {}));
    EXPECT_EQ(0, basis.evalAt0(3, {0, 0}, {}));
    EXPECT_EQ(0, basis.evalAt0(3, {1, 1}, {}));
}

template <int dim>
int find_idx(const Eigen::Matrix<int, dim, Eigen::Dynamic>& powers,
             const Eigen::Matrix<int, dim, 1>& mon) {
    int idx = -1;
    for (int i = 0; i < powers.cols(); ++i) {
        EXPECT_FALSE(powers.col(i) == mon && idx != -1);
        if (powers.col(i) == mon) {
            idx = i;
        }
    }
    EXPECT_NE(idx, -1);
    return idx;
}

TEST(Approximations, Monomials2D) {
    Monomials<Vec2d> basis(9);
    EXPECT_EQ(55, basis.size());

    int c = find_idx(basis.powers(), {0, 0});
    int x = find_idx(basis.powers(), {1, 0});
    int y = find_idx(basis.powers(), {0, 1});
    int xx = find_idx(basis.powers(), {2, 0});
    int xy = find_idx(basis.powers(), {1, 1});
    int yy = find_idx(basis.powers(), {0, 2});

    Range<Vec2d> points = linspace(Vec2d(-1.0), Vec2d(1.0), {30, 30});
    for (auto& p : points) {
        // Values
        ASSERT_DOUBLE_EQ(1, basis(c, p, {0, 0}, {}));
        ASSERT_DOUBLE_EQ(p[1], basis(y, p));
        ASSERT_DOUBLE_EQ(p[0], basis(x, p));
        ASSERT_DOUBLE_EQ(p[1] * p[1], basis(yy, p));
        ASSERT_DOUBLE_EQ(p[0] * p[1], basis(xy, p));
        ASSERT_DOUBLE_EQ(p[0] * p[0], basis(xx, p));
        // x derivatives
        ASSERT_DOUBLE_EQ(0, basis(c, p, {1, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(y, p, {1, 0}, {}));
        ASSERT_DOUBLE_EQ(1, basis(x, p, {1, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(yy, p, {1, 0}, {}));
        ASSERT_DOUBLE_EQ(p[1], basis(xy, p, {1, 0}, {}));
        ASSERT_DOUBLE_EQ(2 * p[0], basis(xx, p, {1, 0}, {}));
        // y derivatives
        ASSERT_DOUBLE_EQ(0, basis(c, p, {0, 1}, {}));
        ASSERT_DOUBLE_EQ(1, basis(y, p, {0, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(x, p, {0, 1}, {}));
        ASSERT_DOUBLE_EQ(2 * p[1], basis(yy, p, {0, 1}, {}));
        ASSERT_DOUBLE_EQ(p[0], basis(xy, p, {0, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(xx, p, {0, 1}, {}));
        // xx derivatives
        ASSERT_DOUBLE_EQ(0, basis(c, p, {2, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(y, p, {2, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(x, p, {2, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(yy, p, {2, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(xy, p, {2, 0}, {}));
        ASSERT_DOUBLE_EQ(2, basis(xx, p, {2, 0}, {}));
        // yy derivatives
        ASSERT_DOUBLE_EQ(0, basis(c, p, {0, 2}, {}));
        ASSERT_DOUBLE_EQ(0, basis(y, p, {0, 2}, {}));
        ASSERT_DOUBLE_EQ(0, basis(x, p, {0, 2}, {}));
        ASSERT_DOUBLE_EQ(2, basis(yy, p, {0, 2}, {}));
        ASSERT_DOUBLE_EQ(0, basis(xy, p, {0, 2}, {}));
        ASSERT_DOUBLE_EQ(0, basis(xx, p, {0, 2}, {}));
        // xy derivatives
        ASSERT_DOUBLE_EQ(0, basis(c, p, {1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(y, p, {1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(x, p, {1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(yy, p, {1, 1}, {}));
        ASSERT_DOUBLE_EQ(1, basis(xy, p, {1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(xx, p, {1, 1}, {}));
    }
}

TEST(Approximations, Monomials3D) {
    Monomials<Vec3d> basis(4);
    EXPECT_EQ(35, basis.size());

    int c = find_idx(basis.powers(), {0, 0, 0});
    int x = find_idx(basis.powers(), {1, 0, 0});
    int y = find_idx(basis.powers(), {0, 1, 0});
    int z = find_idx(basis.powers(), {0, 0, 1});
    int xx = find_idx(basis.powers(), {2, 0, 0});
    int xy = find_idx(basis.powers(), {1, 1, 0});
    int xz = find_idx(basis.powers(), {1, 0, 1});
    int yy = find_idx(basis.powers(), {0, 2, 0});
    int yz = find_idx(basis.powers(), {0, 1, 1});
    int zz = find_idx(basis.powers(), {0, 0, 2});

    Range<Vec3d> points = linspace(Vec3d(-1.0), Vec3d(1.0), {20, 20, 20});
    for (auto& p : points) {
        // Values
        ASSERT_DOUBLE_EQ(1, basis(c, p));
        ASSERT_DOUBLE_EQ(p[2], basis(z, p));
        ASSERT_DOUBLE_EQ(p[1], basis(y, p));
        ASSERT_DOUBLE_EQ(p[0], basis(x, p));
        ASSERT_DOUBLE_EQ(p[2] * p[2], basis(zz, p));
        ASSERT_DOUBLE_EQ(p[2] * p[1], basis(yz, p));
        ASSERT_DOUBLE_EQ(p[1] * p[1], basis(yy, p));
        ASSERT_DOUBLE_EQ(p[0] * p[2], basis(xz, p));
        ASSERT_DOUBLE_EQ(p[0] * p[1], basis(xy, p));
        ASSERT_DOUBLE_EQ(p[0] * p[0], basis(xx, p));
        // x derivatives
        ASSERT_DOUBLE_EQ(0, basis(c, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(z, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(y, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(1, basis(x, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(zz, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(yz, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(0, basis(yy, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(p[2], basis(xz, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(p[1], basis(xy, p, {1, 0, 0}, {}));
        ASSERT_DOUBLE_EQ(2 * p[0], basis(xx, p, {1, 0, 0}, {}));
        // yz derivatives
        ASSERT_DOUBLE_EQ(0, basis(c, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(z, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(y, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(x, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(zz, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(1, basis(yz, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(yy, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(xz, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(xy, p, {0, 1, 1}, {}));
        ASSERT_DOUBLE_EQ(0, basis(xx, p, {0, 1, 1}, {}));
    }
}

TEST(Approximations, MonomialsManual) {
    Monomials<Vec2d> basis({{0, 2}, {1, 2}, {3, 1}, {0, 0}});
    auto points = linspace(Vec2d(-1.0), Vec2d(1.0), {20, 20});
    for (const auto& p : points) {
        EXPECT_DOUBLE_EQ(p[1] * p[1], basis(0, p));
        EXPECT_DOUBLE_EQ(p[0] * p[1] * p[1], basis(1, p));
        EXPECT_DOUBLE_EQ(p[0] * p[0] * p[0] * p[1], basis(2, p));
        EXPECT_DOUBLE_EQ(1, basis(3, p));
    }
}

TEST(Approximations, MonomialsEmpty) {
    Monomials<Vec2d> empty;
    EXPECT_EQ(0, empty.size());
    Monomials<Vec1d> empty1(-1);
    EXPECT_EQ(0, empty1.size());
    EXPECT_EQ(0, Monomials<Vec3d>::tensorBasis(-1).size());
}

TEST(Appoximations, DISABLED_MonomialsUsageExample) {
    /// [Monomials usage example]
    Monomials<Vec2d> basis(2);  // {1, y, x, y^2, xy, x^2}
    std::cout << basis << std::endl;  // Monomials 2D: [0, 0; 0, 1; 1, 0; 0, 2; 1, 1; 2, 0], ...
    double value = basis(2, {0.3, -2.4});  // value = 0.3

    basis = Monomials<Vec2d>::tensorBasis(1);  // tensor basis {1, x, y, xy}
    basis = Monomials<Vec2d>({{1, 0}, {2, 3}});  // from powers
    double der = basis(1, {0.3, -2.4}, {2, 1}, {});  // der = 2*3*(-2.4)^2
    /// [Monomials usage example]
    EXPECT_EQ(2*3*(-2.4)*(-2.4), der);
    EXPECT_EQ(0.3, value);
}

}  // namespace mm
