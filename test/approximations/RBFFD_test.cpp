#include <medusa/bits/approximations/RBFFD.hpp>
#include <medusa/bits/approximations/Gaussian.hpp>
#include <medusa/bits/approximations/Polyharmonic.hpp>
#include <medusa/bits/approximations/RBFBasis.hpp>
#include <medusa/bits/types/Vec.hpp>
#include <Eigen/Cholesky>
#include <Eigen/LU>

#include "gtest/gtest.h"

namespace mm {

TEST(Approximations, RBFFDGauss2D) {
    double h = 0.1123;
    double s = 1.5;

    Gaussian<double> g(s);
    RBFFD<Gaussian<double>, Vec2d> appr(g);

    Range<Vec2d> support = {{0, 0}, {0, h}, {h, 0}, {0, -h}, {-h, 0},
                            {-h, h}, {-h, -h}, {h, -h}, {h, h}};
    appr.compute({0.0, 0.0}, support);

    double a = -4.*(s*s + h*h/std::pow(std::sinh((h/s)*(h/s)), 2)) / std::pow(s, 4);
    double b = 4.*std::exp(3.*(h/s)*(h/s))*h*h / std::pow(-1+std::exp(2*(h/s)*(h/s)), 2)
               / std::pow(s, 4);

    Eigen::VectorXd shape = appr.getShape({{2, 0}}) + appr.getShape({{0, 2}});
    Eigen::VectorXd expected(9); expected << a, b, b, b, b, 0, 0, 0, 0;
    ASSERT_EQ(expected.size(), shape.size());
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_NEAR(expected[i], shape[i], 5e-6);
    }

    double c = 2.*std::exp(3.*(h/s)*(h/s))*h / (-1+std::exp(4*(h/s)*(h/s))) / s / s;
    shape = appr.getShape({{0, 1}});
    expected << 0, c, 0, -c, 0, 0, 0, 0, 0;
    ASSERT_EQ(expected.size(), shape.size());
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_NEAR(expected[i], shape[i], 5e-8);
    }
}

TEST(Approximations, RBFFDGauss2DAugConst) {
    double h = 0.1123;
    double s = 1.5;

    Gaussian<double> g(s);
    RBFFD<Gaussian<double>, Vec2d> appr(g, 0);

    Range<Vec2d> support = {{0, 0}, {0, h}, {h, 0}, {0, -h}, {-h, 0},
                            {-h, h}, {-h, -h}, {h, -h}, {h, h}};
    appr.compute({0.0, 0.0}, support);

    Eigen::VectorXd shape = appr.getShape({{2, 0}}) + appr.getShape({{0, 2}});
    // computed with Mathematica from (quite long) analytical expression
    Eigen::VectorXd expected(9);
    expected << -336.1792004798, 88.49955236455, 88.49955236455, 88.49955236455, 88.49955236455,
            -4.4547522446, -4.4547522446, -4.4547522446, -4.4547522446;
    ASSERT_EQ(expected.size(), shape.size());
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_NEAR(expected[i], shape[i], 5e-6);
    }

    shape = appr.getShape({{0, 1}});
    // same as without a constant
    double c = 2.*std::exp(3.*(h/s)*(h/s))*h / (-1+std::exp(4*(h/s)*(h/s))) / s / s;
    expected << 0, c, 0, -c, 0, 0, 0, 0, 0;
    ASSERT_EQ(expected.size(), shape.size());
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_NEAR(expected[i], shape[i], 5e-8);
    }
}

/*
TEST(Approximations, RBFFD_PHS) {
    Polyharmonic<double, 2> phs;
    RBFFD<decltype(phs), Vec1d, NoScale, Eigen::PartialPivLU<Eigen::MatrixXd>> appr(phs);
    double h = 0.1234;
    Range<Vec1d> supp = {0, -h, h};
    appr.compute({0.0}, supp);
    auto sh = appr.getShape({{2}});
    Eigen::VectorXd expected(supp.size());
    expected << -131.34080574957511249, 65.67040287478755625, 65.67040287478755625;
    for (int i = 0; i < supp.size(); ++i) {
        EXPECT_NEAR(expected[i], sh[i], 1e-16);
    }
}*/

TEST(Approximations, RBFFDGauss2DAugOrd1) {
    double h = 0.1123;
    double s = 1.5;

    Gaussian<double> g(s);
    RBFFD<Gaussian<double>, Vec2d> appr(g, 1);

    Range<Vec2d> support = {{0, 0}, {0, h}, {h, 0}, {0, -h}, {-h, 0},
                            {-h, h}, {-h, -h}, {h, -h}, {h, h}};
    appr.compute({0.0, 0.0}, support);

    Eigen::VectorXd shape = appr.getShape({{2, 0}}) + appr.getShape({{0, 2}});
    // computed with Mathematica from (quite long) analytical expression (same as before)
    Eigen::VectorXd expected(9);
    expected << -336.1792004798, 88.49955236455, 88.49955236455, 88.49955236455, 88.49955236455,
            -4.4547522446, -4.4547522446, -4.4547522446, -4.4547522446;
    ASSERT_EQ(expected.size(), shape.size());
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_NEAR(expected[i], shape[i], 5e-6);
    }

    shape = appr.getShape({{0, 1}});
    // computed with Mathematica from (quite long) analytical expression
    expected << 0, 5.94478246428, 0, -5.94478246428, 0, -0.74621135681,
            0.74621135681, 0.74621135681, -0.74621135681;
    ASSERT_EQ(expected.size(), shape.size());
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_NEAR(expected[i], shape[i], 5e-8);
    }
}

TEST(Approximations, DISABLED_RBFFDUsageExmaple) {
    /// [RBFFD usage example]
    double h = 0.1123;
    double s = 1.5;

    Gaussian<double> g(s);
    // Gaussian RBF's augmented with a constant
    RBFFD<Gaussian<double>, Vec2d> appr(g, Monomials<Vec2d>(0));
    std::cout << appr << std::endl;

    // Local neighbourhood
    Range<Vec2d> support = {{0, 0}, {0, h}, {h, 0}, {0, -h}, {-h, 0},
                            {-h, h}, {-h, -h}, {h, -h}, {h, h}};

    // compute approximations at point `{0.0, 0.0}`.
    appr.compute({0.0, 0.0}, support);

    // Get shape (stencil weights) for approximation of Laplacian.
    Eigen::VectorXd shape = appr.getShape({{2, 0}}) + appr.getShape({{0, 2}});

    // Get shape (stencil weights) for approximation of value
    shape = appr.getShape();
    /// [RBFFD usage example]
}

}  // namespace mm
