#include <medusa/Medusa.hpp>
#include "Eigen/SparseCore"
#include "Eigen/IterativeLinearSolvers"
#include <Eigen/LU>
#include <medusa/bits/domains/BasicRelax.hpp>

#include "gtest/gtest.h"

using namespace mm;  // NOLINT(*)
using namespace std;  // NOLINT(*)

TEST(End2end, DiffusionImplicitPolygon) {
    PolygonShape<Vec2d> poly({{0.0, 0.0}, {3.4, 1.2}, {2.8, 5.8}, {0.2, 3.4}, {-1.2, 2.9}});
    double step = 0.1;
    HDF hdf("test_impl_poly.h5", HDF::DESTROY);
    hdf.close();

    DomainDiscretization<Vec2d> domain = poly.discretizeBoundaryWithStep(step);

    hdf.atomic().writeDomain("domain1", domain);

    GeneralFill<Vec2d> fill;
    fill.proximityTolerance(0.9).seed(1337);
    domain.fill(fill, step);

    BasicRelax relax;
    relax.initialHeat(0.5).finalHeat(0.1).numNeighbours(3)
            .projectionType(BasicRelax::DO_NOT_PROJECT);
    domain.relax(relax, step);

    int N = domain.size();

    domain.findSupport(FindClosest(9));
    RBFFD<Gaussian<double>, Vec2d, ScaleToClosest> approx(1.0, 2);

    auto storage = domain.computeShapes(approx);

    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    M.reserve(storage.supportSizes());
    Eigen::VectorXd rhs(N); rhs.setZero();
    auto op = storage.implicitOperators(M, rhs);

    for (int i : domain.interior()) {
        op.lap(i)*2.0 + 8.0 * op.grad(i, {-1.0, 2.0}) = 1.0;
    }
    for (int i : domain.boundary()) {
        op.value(i) = 0.0;
    }

    Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
    solver.compute(M);
    ScalarFieldd u = solver.solve(rhs);

    hdf.reopen();
    hdf.writeDomain("domain", domain);
    hdf.writeDoubleArray("sol", u);
    hdf.close();
}
